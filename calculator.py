class calc:
    def add(self, strg):
        if strg == "":
            return 0
        else:
            try:
                int(strg)
                if int(strg) < 0:
                    raise Exception("Negative number")
                else:
                    return int(strg)
            except:
                delim = 0
                tmp = strg.replace("\n",",")
                if strg[0] == '/' and strg[1] == '/':
                    delim = strg[2]
                    tmp = tmp.split('/')[-1]
                    tmp = tmp.replace(delim, ",")[1:]
                suma = 0
                if "," in tmp:
                    num = tmp.split(",")
                for number in num:
                    if int(number) < 0:
                        raise Exception("Negative number")
                    if int(number) <= 1000:
                        suma += int(number)
                return suma
                

class testing:
    def test_empty(self):
        x = calc()
        assert x.add("") == 0
    
    def test_nonempty(self):
        x = calc()
        assert x.add("something") != 0

    def test_int(self):
        x = calc()
        assert x.add("13") == int(13)

    def test_sum(self):
        x = calc()
        assert x.add("3,5") == int(8)
    
    def test_space(self):
        x = calc()
        assert x.add("3\n5") == int(8)

    def test_manynum(self):
        x = calc()
        assert x.add("3\n5,1") == int(9)

    def test_negative(self):
        x = calc()
        try:
            x.add("-1")
            return False
        except:
            return True
    
    def test_thou(self):
        x = calc()
        assert x.add("1,1001") == int(1)

    def test_delim(self):
        x = calc()
        assert x.add("//$3$4,1") == int(8)
        

    

t = testing()
t.test_empty()
t.test_int()
t.test_sum()
t.test_space()
t.test_manynum()
t.test_negative()
t.test_thou()
t.test_delim()